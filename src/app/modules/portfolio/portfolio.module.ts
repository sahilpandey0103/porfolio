import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioRoutingModule } from './portfolio-routing.modules';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { ContactComponent } from './pages/contact/contact.component';
import { AboutComponent } from './pages/about/about.component';
import { WorkComponent } from './pages/work/work.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from '@angular/material/list';

const materialModule = [
  MatCardModule,
  MatListModule
]

@NgModule({
  declarations: [
    LandingPageComponent,
    ContactComponent,
    AboutComponent,
    WorkComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    ...materialModule
  ]
})
export class PortfolioModule { }
