import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesRoutingModule } from './modules-routing.module';
import { PortfolioModule } from './portfolio/portfolio.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    PortfolioModule
  ]
})
export class ModulesModule { }
