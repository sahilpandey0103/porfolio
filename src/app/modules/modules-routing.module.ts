import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path : '',
    redirectTo : 'portfolio',
    pathMatch : 'full'
  },
  {
    path : 'portfolio',
    loadChildren : ()=>import('./portfolio/portfolio.module').then(m=>m.PortfolioModule),
  },
  {
    path : '**',
    redirectTo : 'portfolio',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
