import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './modules/portfolio/pages/landing-page/landing-page.component';

const routes: Routes = [
  {
    path : '',
    loadChildren : ()=>import('./modules/modules.module').then(m=>m.ModulesModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
